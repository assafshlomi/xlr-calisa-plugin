# Preface #

This document describes the functionality provided by the XL Release CA Lisa (Nolio) plugin.

See the **XL Release Reference Manual** for background information on XL Release and release concepts.

# Overview #

The xlr-calisa-plugin is a XL Release plugin that allows to execute a deployment using CA Lisa (Nolio).

## Types ##

+ Create Release
+ Run Template
+ Run Release
+ Check Release Status 
